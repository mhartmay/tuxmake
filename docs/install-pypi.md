# Installing from PyPI

**Notes:**

- TuxMake requires Python 3.6 or newer.
- The offline builds feature requires `socat`, consider installing it.

To install TuxMake on your system globally:

```
pip install -U tuxmake
```

To install tuxmake to your home directory at ~/.local/bin:

```
pip install -U --user tuxmake
```

To upgrade TuxMake to the latest version, run the same command you ran to
install it.
