ARG BASE
FROM ${BASE}

ENV DEBIAN_FRONTEND=noninteractive

ARG TC_FULL_VERSION
ARG BUILD_HOST_ARCH
ARG HOSTARCH
ARG INSTALL_OPTIONS
ARG PACKAGES
COPY korg_llvm.sha256sum /

RUN wget --progress=dot:giga https://mirrors.edge.kernel.org/pub/tools/llvm/files/llvm-${TC_FULL_VERSION}-${BUILD_HOST_ARCH}.tar.gz && \
    sha256sum -c korg_llvm.sha256sum --ignore-missing && \
    tar xaf llvm-${TC_FULL_VERSION}-${BUILD_HOST_ARCH}.tar.gz -C /usr/local && \
    rm -f llvm-${TC_FULL_VERSION}-${BUILD_HOST_ARCH}.tar.gz

ENV PATH="$PATH:/usr/local/llvm-${TC_FULL_VERSION}-${BUILD_HOST_ARCH}/bin"

RUN apt-get update -q=2 && \
    gccversion=$(apt-cache policy gcc | sed -e '/Candidate:/!d; s/\s*Candidate:\s*.*://; s/\..*//') && \
    apt-get install -q=2 -y libc6 libgcc-s1 libicu72 liblzma5 libstdc++6 libxml2 libzstd1 zlib1g libc6-dev linux-libc-dev libgcc-${gccversion}-dev libstdc++-${gccversion}-dev

# Install basic toolchain packages. Which toolchain exactly is defined by the
# $PACKAGES build argument
RUN if [ -f /root/tmp_install_options ]; then \
        INSTALL_OPTIONS=$(cat /root/tmp_install_options); \
    fi; \
    # NOTE: Install loongarch64 binutils package from debian unstable since
    #       it is not yet available in debian stable or backports.
    if [ "${HOSTARCH}" = loongarch64 ]; then \
       echo "deb http://deb.debian.org/debian unstable main" >> /etc/apt/sources.list && \
       apt-get update -q=2 && \
       apt-get install -q=2 -y --no-install-recommends \
       --option=debug::pkgProblemResolver=yes \
       -t unstable $PACKAGES; \
    else  \
       apt-get update -q && \
       apt-get install \
       --assume-yes \
       --no-install-recommends \
       --option=debug::pkgProblemResolver=yes \
       ${INSTALL_OPTIONS} \
       $PACKAGES; \
    fi;

# vim: ft=dockerfile
